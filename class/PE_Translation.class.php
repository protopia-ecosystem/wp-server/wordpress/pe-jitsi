<?php 

class PE_Translation extends SMC_Post
{
	static function get_type()
	{
		return PE_TRANSLATION_TYPE;
	}
	static function init()
	{
		add_action('init', 								[ __CLASS__, 'register_all' ], 13);	
		add_action('bio_gq_change_'.PE_TRANSLATION_TYPE,[ __CLASS__, 'bio_gq_change' ], 11, 3);	
		parent::init();
	}
	static function bio_gq_change( $matrix, $obj, $args )
	{
		if(!$args['id'])
		{
			$room = PE_Room::insert(
				[
					"post_title"		=> $args['input']["post_title"],
					"post_name"			=> $args['input']["post_title"],
					"post_content"		=> "",
					PE_TRANSLATION_TYPE	=> $obj->id,
					"is_locked"			=> false
				]
			);
			//wp_die( [ $args, $room ]);
			//$matrix[PE_ROOM_TYPE] = [ PE_Room::get_single_matrix( $room->id )];
		}
		else
		{
				
		}
		return $matrix;
	}
	static function register_all()
	{
		$labels = array(
			'name'               => __("Translation", PEJITSI), // Основное название типа записи
			'singular_name'      => __("Translation", PEJITSI), // отдельное название записи типа Book
			'add_new'            => __("add Translation", PEJITSI), 
			'all_items' 		 => __('Translations', PEJITSI),
			'add_new_item'       => __("add Translation", PEJITSI), 
			'edit_item'          => __("edit Translation", PEJITSI), 
			'new_item'           => __("add Translation", PEJITSI), 
			'view_item'          => __("see Translation", PEJITSI), 
			'search_items'       => __("search Translation", PEJITSI), 
			'not_found'          => __("no Translations", PEJITSI), 
			'not_found_in_trash' => __("no Translations in trash", PEJITSI), 
			'parent_item_colon'  => '',
			'menu_name'          => __("Translations", PEJITSI), 
		);
		register_post_type(
			static::get_type(), 
			[
				'labels'             => $labels,
				'taxonomies'		 => [ ],
				'public'             => true,
				'publicly_queryable' => true,
				'show_ui'            => true,
				'show_in_menu'       => 'pe_jitsi_page',
				'query_var'          => true,
				'capability_type'    => 'post',
				'has_archive'        => true,
				'hierarchical'       => false,
				'menu_position'      => 4,
				"menu_icon"			 => "dashicons-megaphone",
				'supports'           => array( 'title','editor','author','thumbnail','excerpt','comments' ),
				"rewrite"			 => ["slug" => ""]
			]
		);
	}
}